from gevent import monkey
import PyQt5.QtCore  # Importing PyQt5 will force silx to use it
from PyQt5 import QtGui, QtWidgets, QtCore
from silx.gui import qt
from silx.gui.plot import Plot2D
from silx.gui.plot.actions import PlotAction
from matplotlib import pyplot
import matplotlib.pyplot as plt
from skimage import morphology
from skimage import measure
import numpy as np
import scipy
import fabio
import time
import cv2
import sys
import time
import math


class OffsetsCalculation:
	
	def __init__(self,resolution,imageDir,stand,borderType):
		
		#monkey.patch_all(thread=False)

		self.totalProcessingTime = 0
		
		#self.sampleSizeX = sampleSize[0]
		#self.sampleSizeY = sampleSize[1]
		#self.sampleSizeZ = sampleSize[2]

		#self.pathImg = str(imageDir)+'center0000.edf'
		#self.pathFlat = str(imageDir)+'center0001.edf'
		#self.pathDark = str(imageDir)+'center0002.edf'
		self.pathImg = '/users/muzelle/Desktop/success2/007_HA-300_6.5_NUS2__004_/007_HA-300_6.5_NUS2__004_0000.edf'
		self.pathFlat = '/users/muzelle/Desktop/success2/007_HA-300_6.5_NUS2__004_/refHST0000.edf'
		self.pathDark = '/users/muzelle/Desktop/success2/007_HA-300_6.5_NUS2__004_/dark.edf'
		
		self.resolution = resolution
		
		self.stand = stand
		
		self.borderType = borderType
		
		self.img = None
		self.flat = None
		self.dark = None
		
		self.dispImg = None
		self.intImg = None
		
		self.centImgY = None
		self.centImgX = None
		
		self.centerY = None
		self.centerX = None
		
		self.leftBorder = None
		self.rightBorder = None
		
		self.borderDetected = "false"
		

	def imagesOpening(self):
		
		start = time.time()

		self.img = fabio.open(self.pathImg) 
		self.flat = fabio.open(self.pathFlat)
		self.dark = fabio.open(self.pathDark)

		self.centImgY = int(self.img.data.shape[0]/2)
		self.centImgX = int(self.img.data.shape[1]/2)
		
		imagesOpeningTime = time.time()-start
		self.totalProcessingTime += imagesOpeningTime
		print("Time to open images: "+str(imagesOpeningTime))
		
		
	def imageContrastEnhancement(self):
		
		start = time.time()
		
		filtImg = np.subtract(self.img.data,self.dark.data).astype(np.float32)
		filtImgFlat = np.subtract(self.flat.data,self.dark.data).astype(np.float32)
		corrImg = np.divide(filtImg,filtImgFlat).astype(np.float32)
		corrImg[corrImg < 0] = 0
		
		enhImg = cv2.normalize(corrImg, None, 0, 255, cv2.NORM_MINMAX).astype(np.uint8)
		
		self.img = corrImg
		
		imageContrastEnhancementTime = time.time()-start
		self.totalProcessingTime += imageContrastEnhancementTime
		print("Time to enhance image contrast: "+str(imageContrastEnhancementTime))
		
	def imageFiltering(self):
			
		start = time.time()

		blurImg = cv2.bilateralFilter(self.img,9,75,75)
		blurImg = cv2.normalize(blurImg, None, 0, 255, cv2.NORM_MINMAX).astype(np.uint8)
		
		self.img = blurImg
		self.dispImg = blurImg
		
		imageFilteringTime = time.time()-start
		self.totalProcessingTime += imageFilteringTime
		print("Time to filter image: "+str(imageFilteringTime))
		
	def edgesDetection(self):

		start = time.time()

		edges = cv2.Laplacian(self.img,cv2.CV_64F,ksize=5)
		edges = cv2.GaussianBlur(edges,(3,3),0)

		kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
		edges = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)

		thr = np.median(edges)*1.33

		edges = np.uint8(edges < -thr)
		
		#measures = cv2.connectedComponentsWithStats(edges, 8, cv2.CV_32S)
		
		#labImg = measures[1]
		
		#areas = measures[2][:,cv2.CC_STAT_AREA]
		
		labImg = measure.label(edges, neighbors=8, background=0)

		labImg += 1 

		props = measure.regionprops(labImg)

		areas = [p.area for p in props]

		noise = np.mean(areas)*5

		labImg = morphology.remove_small_objects(labImg,noise,in_place=True)

		edges = np.uint8(labImg > 0)
		
		#self.dispImg[edges > 0] = 0
		
		#savedImg = fabio.edfimage.EdfImage()
		#savedImg.data = self.dispImg
		#savedImg.write('/segfs/tango/tmp/clemence/SampleAlignment/edgesDetected.edf')
		
		self.img = labImg
		
		edgesDetectionTime = time.time()-start
		self.totalProcessingTime += edgesDetectionTime
		print("Time to find edges: "+str(edgesDetectionTime))
		
		
	def sampleCenterXCalculation(self):
		
		start = time.time()
		
		props = measure.regionprops(self.img,self.dispImg)
		
		centroids = [(p.centroid[0],p.centroid[1]) for p in props if p.max_intensity > 200]
		
		if len(centroids) != 0:
			self.borderDetected = "true"
				
			if self.stand == 'absent':
				
						
				if self.borderType == 'straight':
							
					self.leftBorder = np.min(centroids,axis=0)[1]

					self.rightBorder = np.max(centroids,axis=0)[1]
							
					self.centerX = int((self.rightBorder - self.leftBorder)/2+self.leftBorder)			
				
				if self.borderType == 'irregular':
					
					edges = self.img > 0
					
					self.leftBorder = np.min(centroids,axis=0)[1]
					
					print('left border: '+str(self.leftBorder))
					
					self.rightBorder = np.max(centroids,axis=0)[1]
					
					print('right border: '+str(self.rightBorder))
					
					self.centerX = int(np.mean([(np.min(np.where(edges[raw,:])[0])+np.max(np.where(edges[raw,:])[0]))/2
														for raw in range(edges.shape[0]) 
														if len(np.where(edges[raw,:])[0]) != 0 
														and (np.max(np.where(edges[raw,:])[0])-np.min(np.where(edges[raw,:])[0])) > 200]
														))
														
				if self.borderType == 'oblique':
					
					edges = self.img > 0
						
					areas = [p.area for p in props]
						
					fullBorder = [p.label for p in props if p.area == np.max(areas)][0]
							
					centerRawByRaw = np.array([((np.min(np.where(edges[raw,:])[0])+np.max(np.where(edges[raw,:])[0]))/2,raw)
									for raw in range(edges.shape[0]) 
									if len(np.where(edges[raw,:])[0]) != 0 and 
									(np.max(np.where(edges[raw,:])[0])-np.min(np.where(edges[raw,:])[0])) > 100])
							
					meanCenter = np.median(centerRawByRaw,axis=0)
					
					rows = np.where(self.img == fullBorder)[0]
					cols = np.where(self.img == fullBorder)[1]
					
					ya = np.mean(rows[np.where(cols == np.min(cols))])
					yb = np.mean(rows[np.where(cols == np.max(cols))])
					xb = np.max(cols)
					xa = np.min(cols)
					
					slope = (yb-ya)/(xb-xa)
					
					self.centerX = (self.centImgY-meanCenter[1])/slope + meanCenter[0]
			 
			offsetSX = (self.centImgX-self.centerX)*self.resolution*10**-3
				
		
		centerXCalculationTime = time.time()-start
		self.totalProcessingTime += centerXCalculationTime
		
		print("Time to calculate sample center X: "+str(centerXCalculationTime))
		
		return offsetSX
		
		
	def sampleCenterYCalculation(self):
		pass
		
	def localTomoCenterXCalculation(self):
		
		start = time.time()
		
		props = measure.regionprops(self.img,self.intImg)
		
		areas = [p.area for p in props]
					
		border = [p.centroid for p in props if p.area == np.max(areas) and p.max_intensity > 200]
				
		if len(border) == 0:
					
			offsetSX = self.img.shape[1]
				
		else:
					
			ret,binImg = cv2.threshold(self.dispImg,0,1,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
					
			labImg = measure.label(binImg, neighbors=8, background=0)

			labImg += 1 

			props = measure.regionprops(labImg)
					
			areas = [p.area for p in props]
					
			sample = [p.centroid for p in props if p.area == np.max(areas)][0]
					
			if sample[1] > border[0][1]:
						
				self.borderDetected = "right"
						
			else:	
				self.borderDetected = "left"
			
			self.centerX = border[0][1]
			
			offsetSX = (self.centImgX-self.centerX)*self.resolution*10**-3
		
		centerXCalculationTime = time.time()-start
		self.totalProcessingTime += centerXCalculationTime
		
		print("Time to calculate sample center X: "+str(centerXCalculationTime))
			
		return offsetSX

	def displayResults(self):
		
		try:
			self.centerX
		except NameError:
			self.centerX = 0
		try:
			self.centerY
		except NameError:
			self.centerY = 0
			
		print('\n')
		print('total processing time: '+str(self.totalProcessingTime))
		print('\n')		

		print('Sample center detected: '+str(self.centerX)+' in X and: '+str(self.centerY)+' in Y')
		print('Image center: '+str(self.centImgX)+' in X and: '+str(self.centImgY)+' in Y')
		
	def displayImage(self):
		
		edgesDetection = fabio.open('/segfs/tango/tmp/clemence/SampleAlignment/edgesDetected.edf')
		
		qapp = qt.QApplication([])
		plot = Plot2D()
		plot.addImage(edgesDetection.data, legend='image')
		plot.show()
		qapp.exec_()
		
if __name__ == '__main__':
	
	test = OffsetsCalculation(12.57,"/segfs/tango/tmp/clemence/SampleAlignment/","absent","straight")
	test.imagesOpening()
	test.imageContrastEnhancement()
	test.imageFiltering()
	test.edgesDetection()
	#test.localTomoCenterXCalculation()
	#test.sampleCenterXCalculation()
	#test.displayResults()
	#inst = OffsetsCalculation(0,"","","")
	#inst.displayImage()	
		
		
