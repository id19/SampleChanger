# -*- coding: utf-8 -*-
#
# This file is part of the TomoSampleAlign project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Tomo Sample Alignment

"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(TomoSampleAlign.additionnal_import) ENABLED START #

import sys
import time
import math
import gevent
import traceback

from SampleAlignment import OffsetsCalculation

# PROTECTED REGION END #    //  TomoSampleAlign.additionnal_import

__all__ = ["TomoSampleAlign", "main"]


class TomoSampleAlign(Device):
    """
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(TomoSampleAlign.class_variable) ENABLED START #
    
    green_mode = PyTango.GreenMode.Gevent
    
    #
    # Blocking operation to find sample center
    #
    def alignment_task(self):
        try:
            self.info_stream("started alignment")
        
            self.is_error  = False
            self.is_moving = True
        
			# alignment code
            print ("Alignment start\n")
            
            #gevent.sleep(3)
            
            align = OffsetsCalculation(self.attr_Resolution_read,self.attr_ImageDirectory_read,self.attr_StandDetection_read,self.attr_BorderType_read)
            align.imagesOpening()
            align.imageContrastEnhancement()
            align.imageFiltering()
            align.edgesDetection()
            
            if self.attr_AlignX_read:
				if self.attr_LocalTomo_read == True:
					offsetSX = align.localTomoCenterXCalculation()
					self.attr_BorderDetected_read = align.borderDetected
					
					print(self.attr_BorderDetected_read)
				else:
					offsetSX = align.sampleCenterXCalculation()
            else:
				offsetSX = 0
            if self.attr_AlignY_read:
				offsetSY = align.sampleCenterYCalculation()
            else:
				offsetSY = 0
            
            align.displayResults()
 
            
            print('Offset detected: '+str(int(offsetSX/(self.attr_Resolution_read*10**-3)))+' in X and: '+str(int(offsetSY/(self.attr_Resolution_read*10**-3)))+' in Y')
            print('Relative SY moving of '+str(offsetSX))
            print('Relative SZ moving of '+str(offsetSY))
            
            self.attr_AlignmentOffsets_read = [offsetSX,offsetSY]
            
            self.is_moving = False
            
            self.info_stream("\nfinished alignment")
            
            
        
        # catch all exceptions, print the traceback to the console and
        # add the information to the device status until acknowledgement
        except:
            ex_info = sys.exc_info()
            try:
                PyTango.Except.throw_exception(str(ex_info[0]), str(ex_info[1]), traceback.format_exc())
            except PyTango.DevFailed as ex:
                self.task_error = ex
                PyTango.Except.print_exception(ex)
                    
                self.is_error  = True
                self.is_moving = False
    
    # PROTECTED REGION END #    //  TomoSampleAlign.class_variable

    # ----------
    # Attributes
    # ----------

    SampleSizeZ = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    SampleSizeY = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    SampleSizeX = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    Resolution = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    BorderType = attribute(
        dtype='str',
        access=AttrWriteType.READ_WRITE,
    )

    StandDetection = attribute(
        dtype='str',
        access=AttrWriteType.READ_WRITE,
    )

    AlignX = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
    )

    AlignY = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
    )

    ImageDirectory = attribute(
        dtype='str',
        access=AttrWriteType.READ_WRITE,
    )

    LocalTomo = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
    )

    BorderDetected = attribute(
        dtype='str',
    )

    AlignmentOffsets = attribute(
        dtype=('double',),
        max_dim_x=2,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(TomoSampleAlign.init_device) ENABLED START #
        # init flags for state and status
        
        self.is_moving = False
        self.is_error = False
        
        self.running_task = None
        self.task_error = None
        
        self.attr_SampleSizeZ_read = 0.0
        self.attr_SampleSizeY_read = 0.0
        self.attr_SampleSizeX_read = 0.0
        self.attr_Resolution_read = None
        self.attr_BorderType_read = "None"
        self.attr_StandDetection_read = "None"
        self.attr_ImageDirectory_read = "None"
        self.attr_AlignX_read = False
        self.attr_AlignY_read = False
        self.attr_AlignmentOffsets_read = [0.0,0.0]
        print("done!")
        
        # PROTECTED REGION END #    //  TomoSampleAlign.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(TomoSampleAlign.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSampleAlign.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(TomoSampleAlign.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSampleAlign.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_SampleSizeZ(self):
        # PROTECTED REGION ID(TomoSampleAlign.SampleSizeZ_read) ENABLED START #
        return self.attr_SampleSizeZ_read
        # PROTECTED REGION END #    //  TomoSampleAlign.SampleSizeZ_read

    def write_SampleSizeZ(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.SampleSizeZ_write) ENABLED START #
        self.attr_SampleSizeZ_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.SampleSizeZ_write

    def is_SampleSizeZ_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_SampleSizeZ_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_SampleSizeZ_allowed

    def read_SampleSizeY(self):
        # PROTECTED REGION ID(TomoSampleAlign.SampleSizeY_read) ENABLED START #
        return self.attr_SampleSizeY_read
        # PROTECTED REGION END #    //  TomoSampleAlign.SampleSizeY_read

    def write_SampleSizeY(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.SampleSizeY_write) ENABLED START #
        self.attr_SampleSizeY_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.SampleSizeY_write

    def is_SampleSizeY_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_SampleSizeY_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_SampleSizeY_allowed

    def read_SampleSizeX(self):
        # PROTECTED REGION ID(TomoSampleAlign.SampleSizeX_read) ENABLED START #
        return self.attr_SampleSizeX_read
        # PROTECTED REGION END #    //  TomoSampleAlign.SampleSizeX_read

    def write_SampleSizeX(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.SampleSizeX_write) ENABLED START #
        self.attr_SampleSizeX_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.SampleSizeX_write

    def is_SampleSizeX_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_SampleSizeX_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_SampleSizeX_allowed

    def read_Resolution(self):
        # PROTECTED REGION ID(TomoSampleAlign.Resolution_read) ENABLED START #
        return self.attr_Resolution_read
        # PROTECTED REGION END #    //  TomoSampleAlign.Resolution_read

    def write_Resolution(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.Resolution_write) ENABLED START #
        self.attr_Resolution_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.Resolution_write

    def is_Resolution_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_Resolution_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_Resolution_allowed

    def read_BorderType(self):
        # PROTECTED REGION ID(TomoSampleAlign.BorderType_read) ENABLED START #
        return self.attr_BorderType_read
        # PROTECTED REGION END #    //  TomoSampleAlign.BorderType_read

    def write_BorderType(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.BorderType_write) ENABLED START #
        self.attr_BorderType_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.BorderType_write

    def is_BorderType_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_BorderType_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_BorderType_allowed

    def read_StandDetection(self):
        # PROTECTED REGION ID(TomoSampleAlign.StandDetection_read) ENABLED START #
        return self.attr_StandDetection_read
        # PROTECTED REGION END #    //  TomoSampleAlign.StandDetection_read

    def write_StandDetection(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.StandDetection_write) ENABLED START #
        self.attr_StandDetection_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.StandDetection_write

    def is_StandDetection_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_StandDetection_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_StandDetection_allowed

    def read_AlignX(self):
        # PROTECTED REGION ID(TomoSampleAlign.AlignX_read) ENABLED START #
        return self.attr_AlignX_read
        # PROTECTED REGION END #    //  TomoSampleAlign.AlignX_read

    def write_AlignX(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.AlignX_write) ENABLED START #
        self.attr_AlignX_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.AlignX_write

    def is_AlignX_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_AlignX_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_AlignX_allowed

    def read_AlignY(self):
        # PROTECTED REGION ID(TomoSampleAlign.AlignY_read) ENABLED START #
        return self.attr_AlignY_read
        # PROTECTED REGION END #    //  TomoSampleAlign.AlignY_read

    def write_AlignY(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.AlignY_write) ENABLED START #
        self.attr_AlignY_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.AlignY_write

    def is_AlignY_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_AlignY_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_AlignY_allowed

    def read_ImageDirectory(self):
        # PROTECTED REGION ID(TomoSampleAlign.ImageDirectory_read) ENABLED START #
        return self.attr_ImageDirectory_read
        # PROTECTED REGION END #    //  TomoSampleAlign.ImageDirectory_read

    def write_ImageDirectory(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.ImageDirectory_write) ENABLED START #
        self.attr_ImageDirectory_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.ImageDirectory_write

    def is_ImageDirectory_allowed(self, attr):
        # PROTECTED REGION ID(TomoSampleAlign.is_ImageDirectory_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_ImageDirectory_allowed

    def read_LocalTomo(self):
        # PROTECTED REGION ID(TomoSampleAlign.LocalTomo_read) ENABLED START #
        return self.attr_LocalTomo_read
        # PROTECTED REGION END #    //  TomoSampleAlign.LocalTomo_read

    def write_LocalTomo(self, value):
        # PROTECTED REGION ID(TomoSampleAlign.LocalTomo_write) ENABLED START #
        self.attr_LocalTomo_read = value
        # PROTECTED REGION END #    //  TomoSampleAlign.LocalTomo_write

    def read_BorderDetected(self):
        # PROTECTED REGION ID(TomoSampleAlign.BorderDetected_read) ENABLED START #
        return self.attr_BorderDetected_read
        # PROTECTED REGION END #    //  TomoSampleAlign.BorderDetected_read

    def read_AlignmentOffsets(self):
        # PROTECTED REGION ID(TomoSampleAlign.AlignmentOffsets_read) ENABLED START #
        return self.attr_AlignmentOffsets_read
        # PROTECTED REGION END #    //  TomoSampleAlign.AlignmentOffsets_read


    # --------
    # Commands
    # --------

    @DebugIt()
    def dev_state(self):
        # PROTECTED REGION ID(TomoSampleAlign.State) ENABLED START #
        if self.attr_Resolution_read == None or self.attr_BorderType_read == "None" \
        or self.attr_StandDetection_read == "None" or self.attr_ImageDirectory_read == "None" \
        or (self.attr_AlignX_read == False and self.attr_AlignY_read == False):
            _state = PyTango.DevState.FAULT
        elif self.is_moving == True:
			_state = PyTango.DevState.MOVING
        else:
            _state = PyTango.DevState.ON
        
        self.set_state(_state)  
        return _state
        # PROTECTED REGION END #    //  TomoSampleAlign.State

    @DebugIt()
    def dev_status(self):
        # PROTECTED REGION ID(TomoSampleAlign.Status) ENABLED START #
        state = self.dev_state()
        
        self._status = "The device state is "
        if state == PyTango.DevState.FAULT:
            self._status += "FAULT"
            if self.attr_AlignX_read == False and self.attr_AlignY_read == False:
				self._status += "\nNone alignment direction is specified. Please enter in which direction (X,Y or both)"\
				" you want to align the sample"
            if self.attr_ImageDirectory_read == "None":
				self._status += "\nAccess path to images to process is not specified. Please enter it"
            if self.attr_StandDetection_read == "None":
				self._status += "\nPresence or absence of stand in the field of view is not specified. Please enter it"            
            if self.attr_BorderType_read == "None":
				self._status += "\nNo borders type specified. Please enter the type of borders to be detected"
            if self.attr_Resolution_read == None:
				self._status += "\nNo resolution specified. Please enter image resolution value"
        elif state == PyTango.DevState.MOVING:
			self._status += "MOVING"
        else:
            self._status += "ON"
        
        self.set_status(self._status)
        return self._status
        # PROTECTED REGION END #    //  TomoSampleAlign.Status

    @command(
    )
    @DebugIt()
    def AlignSample(self):
        # PROTECTED REGION ID(TomoSampleAlign.AlignSample) ENABLED START #
        
        self.running_task = gevent.spawn(self.alignment_task)
        print("Running started")
        self.is_moving = True
		
        
        # PROTECTED REGION END #    //  TomoSampleAlign.AlignSample

    def is_AlignSample_allowed(self):
        # PROTECTED REGION ID(TomoSampleAlign.is_AlignSample_allowed) ENABLED START #
        return self.get_state() not in [DevState.MOVING]
        # PROTECTED REGION END #    //  TomoSampleAlign.is_AlignSample_allowed

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(TomoSampleAlign.main) ENABLED START #
    
    # Enable gevents for the server
    kwargs.setdefault("green_mode", PyTango.GreenMode.Gevent)
    
    return run((TomoSampleAlign,), args=args, **kwargs)
    # PROTECTED REGION END #    //  TomoSampleAlign.main

if __name__ == '__main__':
    main()
