import tango
import gevent
from tango.server import Device
from tango import DevState
from tango.test_context import DeviceTestContext

from TomoSampleAlign import TomoSampleAlign

def test_simple():
    with DeviceTestContext(TomoSampleAlign) as proxy:
        proxy.alignX = True
        proxy.resolution = 12.57
        proxy.standDetection = "absent"
        proxy.borderType = "straight"
        proxy.imageDirectory = "/segfs/tango/tmp/clemence/SampleAlignment/"
        proxy.localTomo = False
        assert proxy.state() == tango.DevState.ON
        proxy.alignSample()
        while proxy.state() == tango.DevState.MOVING:
            gevent.sleep(0.001)
        assert proxy.state() == tango.DevState.ON    
        assert proxy.alignmentOffsets[0] != 0.0
