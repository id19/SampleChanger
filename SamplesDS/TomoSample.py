# -*- coding: utf-8 -*-
#
# This file is part of the TomoSample project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Tomo Sample changer

Definition for one sample in the sample changer
"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(TomoSample.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  TomoSample.additionnal_import

__all__ = ["TomoSample", "main"]


class TomoSample(Device):
    """
    Definition for one sample in the sample changer
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(TomoSample.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  TomoSample.class_variable

    # ----------
    # Attributes
    # ----------

    Position = attribute(
        dtype='str',
    )

    Name = attribute(
        dtype='str',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    SizeX = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    SizeY = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    SizeZ = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    AlignSX = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    AlignSY = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    AlignSZ = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )

    Aligned = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
        doc="The sample was aligned on the tomograph and has its alignment positions stored.",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(TomoSample.init_device) ENABLED START #
        
        self.attr_Name_read     = "None"
        self.attr_Position_read = "None"
        self.attr_SizeX_read    = 0.0
        self.attr_SizeY_read    = 0.0
        self.attr_SizeZ_read    = 0.0
        self.attr_AlignSX_read  = 0.0
        self.attr_AlignSY_read  = 0.0
        self.attr_AlignSZ_read  = 0.0
        self.attr_Aligned_read  = False
        
        #
        # Get sample changer position from the device name
        dev_name = self.get_name()
        dom, fam, member = dev_name.split("/")
        self.attr_Position_read = member
        
        # PROTECTED REGION END #    //  TomoSample.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(TomoSample.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSample.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(TomoSample.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSample.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_Position(self):
        # PROTECTED REGION ID(TomoSample.Position_read) ENABLED START #
        return self.attr_Position_read
        # PROTECTED REGION END #    //  TomoSample.Position_read

    def read_Name(self):
        # PROTECTED REGION ID(TomoSample.Name_read) ENABLED START #
        return self.attr_Name_read
        # PROTECTED REGION END #    //  TomoSample.Name_read

    def write_Name(self, value):
        # PROTECTED REGION ID(TomoSample.Name_write) ENABLED START #
        self.attr_Name_read = value
        # PROTECTED REGION END #    //  TomoSample.Name_write

    def read_SizeX(self):
        # PROTECTED REGION ID(TomoSample.SizeX_read) ENABLED START #
        return self.attr_SizeX_read
        # PROTECTED REGION END #    //  TomoSample.SizeX_read

    def write_SizeX(self, value):
        # PROTECTED REGION ID(TomoSample.SizeX_write) ENABLED START #
        self.attr_SizeX_read = value
        # PROTECTED REGION END #    //  TomoSample.SizeX_write

    def read_SizeY(self):
        # PROTECTED REGION ID(TomoSample.SizeY_read) ENABLED START #
        return self.attr_SizeY_read
        # PROTECTED REGION END #    //  TomoSample.SizeY_read

    def write_SizeY(self, value):
        # PROTECTED REGION ID(TomoSample.SizeY_write) ENABLED START #
        self.attr_SizeY_read = value
        # PROTECTED REGION END #    //  TomoSample.SizeY_write

    def read_SizeZ(self):
        # PROTECTED REGION ID(TomoSample.SizeZ_read) ENABLED START #
        return self.attr_SizeZ_read
        # PROTECTED REGION END #    //  TomoSample.SizeZ_read

    def write_SizeZ(self, value):
        # PROTECTED REGION ID(TomoSample.SizeZ_write) ENABLED START #
        self.attr_SizeZ_read = value
        # PROTECTED REGION END #    //  TomoSample.SizeZ_write

    def read_AlignSX(self):
        # PROTECTED REGION ID(TomoSample.AlignSX_read) ENABLED START #
        return self.attr_AlignSX_read
        # PROTECTED REGION END #    //  TomoSample.AlignSX_read

    def write_AlignSX(self, value):
        # PROTECTED REGION ID(TomoSample.AlignSX_write) ENABLED START #
        self.attr_AlignSX_read = value
        # PROTECTED REGION END #    //  TomoSample.AlignSX_write

    def read_AlignSY(self):
        # PROTECTED REGION ID(TomoSample.AlignSY_read) ENABLED START #
        return self.attr_AlignSY_read
        # PROTECTED REGION END #    //  TomoSample.AlignSY_read

    def write_AlignSY(self, value):
        # PROTECTED REGION ID(TomoSample.AlignSY_write) ENABLED START #
        self.attr_AlignSY_read = value
        # PROTECTED REGION END #    //  TomoSample.AlignSY_write

    def read_AlignSZ(self):
        # PROTECTED REGION ID(TomoSample.AlignSZ_read) ENABLED START #
        return self.attr_AlignSZ_read
        # PROTECTED REGION END #    //  TomoSample.AlignSZ_read

    def write_AlignSZ(self, value):
        # PROTECTED REGION ID(TomoSample.AlignSZ_write) ENABLED START #
        self.attr_AlignSZ_read = value
        # PROTECTED REGION END #    //  TomoSample.AlignSZ_write

    def read_Aligned(self):
        # PROTECTED REGION ID(TomoSample.Aligned_read) ENABLED START #
        return self.attr_Aligned_read
        # PROTECTED REGION END #    //  TomoSample.Aligned_read

    def write_Aligned(self, value):
        # PROTECTED REGION ID(TomoSample.Aligned_write) ENABLED START #
        self.attr_Aligned_read = value
        # PROTECTED REGION END #    //  TomoSample.Aligned_write


    # --------
    # Commands
    # --------

    @DebugIt()
    def dev_state(self):
        # PROTECTED REGION ID(TomoSample.State) ENABLED START #
        
        if self.attr_Name_read == "None":
            _state = PyTango.DevState.DISABLE
        else:
            _state = PyTango.DevState.ON
        
        self.set_state(_state)  
        return _state
        # PROTECTED REGION END #    //  TomoSample.State

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(TomoSample.Reset) ENABLED START #
        
        myself = PyTango.DeviceProxy (self.get_name())
        myself.write_attribute_asynch("Name" , "None")
        myself.write_attribute_asynch("SizeX" , 0)
        myself.write_attribute_asynch("SizeY" , 0)
        myself.write_attribute_asynch("SizeZ" , 0)
        myself.write_attribute_asynch("AlignSX" , 0)
        myself.write_attribute_asynch("AlignSY" , 0)
        myself.write_attribute_asynch("AlignSZ" , 0)
        myself.write_attribute_asynch("Aligned" , False)
        
        # PROTECTED REGION END #    //  TomoSample.Reset

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(TomoSample.main) ENABLED START #
    return run((TomoSample,), args=args, **kwargs)
    # PROTECTED REGION END #    //  TomoSample.main

if __name__ == '__main__':
    main()
