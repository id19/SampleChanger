# -*- coding: utf-8 -*-
#
# This file is part of the TomoSamples project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Tomo Sample changer

"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import device_property
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(TomoSamples.additionnal_import) ENABLED START #

import sys
import TomoSample

# PROTECTED REGION END #    //  TomoSamples.additionnal_import

__all__ = ["TomoSamples", "main"]


class TomoSamples(Device):
    """
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(TomoSamples.class_variable) ENABLED START #
            
    # PROTECTED REGION END #    //  TomoSamples.class_variable

    # -----------------
    # Device Properties
    # -----------------

    SamplePath = device_property(
        dtype='str', default_value="id19/tomo-sample/"
    )

    ProtectedSampleNames = device_property(
        dtype=('str',), default_value=[[None]]
    )

    # ----------
    # Attributes
    # ----------

    ActiveSampleName = attribute(
        dtype='str',
    )

    ActiveSamplePosition = attribute(
        dtype='str',
    )

    ActiveAligned = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
    )

    Samples = attribute(
        dtype=('str',),
        max_dim_x=104,
    )

    ActiveSampleSize = attribute(
        dtype=('double',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=3,
    )

    ActiveSampleAlignPos = attribute(
        dtype=('double',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=3,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(TomoSamples.init_device) ENABLED START #
        
        self.sample_devices = {}
        self.tomo_samples   = {}
        
        self.active_sample_name     = "None"
        self.active_sample_position = "None"
        
        for col in ["A", "B", "C"]:
            for n in range(1,19):
                pos = "%s%d" % (col, n)
                # The samples A7 and A8 do not exist on the sample changer shelf
                if pos == "A7" or pos == "A8" or pos == "B7" or pos == "B8" or pos == "C7" or pos == "C8":
                    continue
                
                dev_name = self.SamplePath + pos
                dev_proxy = PyTango.DeviceProxy (dev_name)
                self.sample_devices[pos] = dev_proxy
                
                sample_name = dev_proxy.read_attribute("Name").value
                if sample_name != "None":
                    self.tomo_samples[pos] = sample_name
            
        print(self.tomo_samples)
            
        
        
        
        # PROTECTED REGION END #    //  TomoSamples.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(TomoSamples.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSamples.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(TomoSamples.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSamples.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_ActiveSampleName(self):
        # PROTECTED REGION ID(TomoSamples.ActiveSampleName_read) ENABLED START #
        return self.active_sample_name
        # PROTECTED REGION END #    //  TomoSamples.ActiveSampleName_read

    def is_ActiveSampleName_allowed(self, attr):
        # PROTECTED REGION ID(TomoSamples.is_ActiveSampleName_allowed) ENABLED START #
        return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_ActiveSampleName_allowed

    def read_ActiveSamplePosition(self):
        # PROTECTED REGION ID(TomoSamples.ActiveSamplePosition_read) ENABLED START #
        return self.active_sample_position
        # PROTECTED REGION END #    //  TomoSamples.ActiveSamplePosition_read

    def is_ActiveSamplePosition_allowed(self, attr):
        # PROTECTED REGION ID(TomoSamples.is_ActiveSamplePosition_allowed) ENABLED START #
        return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_ActiveSamplePosition_allowed

    def read_ActiveAligned(self):
        # PROTECTED REGION ID(TomoSamples.ActiveAligned_read) ENABLED START #
        
        if self.active_sample_position == "None":
            err_msg = "No active sample defined!"
            PyTango.Except.throw_exception("ReadError", err_msg, "read_ActiveAligned")
            
        aligned = False
        aligned = self.sample_devices[self.active_sample_position].read_attribute("Aligned").value
        return aligned
        
        # PROTECTED REGION END #    //  TomoSamples.ActiveAligned_read

    def write_ActiveAligned(self, value):
        # PROTECTED REGION ID(TomoSamples.ActiveAligned_write) ENABLED START #
        
        if self.active_sample_position == "None":
            err_msg = "No active sample defined!"
            PyTango.Except.throw_exception("WriteError", err_msg, "write_ActiveAligned")
        
        self.sample_devices[self.active_sample_position].write_attribute("Aligned", value)
        
        # PROTECTED REGION END #    //  TomoSamples.ActiveAligned_write

    def is_ActiveAligned_allowed(self, attr):
        # PROTECTED REGION ID(TomoSamples.is_ActiveAligned_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.DISABLE]
        else:
            return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_ActiveAligned_allowed

    def read_Samples(self):
        # PROTECTED REGION ID(TomoSamples.Samples_read) ENABLED START #
        
        numbermap = {'A1': 1, 'A2': 2, 'A3': 3, 'A4': 4, 'A5': 5, 'A6': 6, 'A9': 7, 'A10': 8, 'A11': 9, 'A12': 10, 'A13': 11, 'A14': 12, 'A15': 13, 'A16': 14, 'A17': 15, 'A18': 16, \
                     'B1': 17, 'B2': 18, 'B3': 19, 'B4': 20, 'B5': 21, 'B6': 22, 'B9': 23, 'B10': 24, 'B11': 25, 'B12': 26, 'B13': 27, 'B14': 28, 'B15': 29, 'B16': 30, 'B17': 31, 'B18': 32, \
                     'C1': 33, 'C2': 34, 'C3': 35, 'C4': 36, 'C5': 37, 'C6': 38, 'C9': 39, 'C10': 40, 'C11': 41, 'C12': 42, 'C13': 43, 'C14': 44, 'C15': 45, 'C16': 46, 'C17': 47, 'C18': 48}
        
        samplelist = []
        #for pos in sorted(self.tomo_samples.keys()):
        for pos in sorted(self.tomo_samples, key=numbermap.__getitem__):
            samplelist.append(pos)
            samplelist.append(self.tomo_samples[pos])
        return samplelist
        
        # PROTECTED REGION END #    //  TomoSamples.Samples_read

    def is_Samples_allowed(self, attr):
        # PROTECTED REGION ID(TomoSamples.is_Samples_allowed) ENABLED START #
        return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_Samples_allowed

    def read_ActiveSampleSize(self):
        # PROTECTED REGION ID(TomoSamples.ActiveSampleSize_read) ENABLED START #
        
        if self.active_sample_position == "None":
            err_msg = "No active sample defined!"
            PyTango.Except.throw_exception("ReadError", err_msg, "read_ActiveSampleSize")
            
        sample_size = [0.0, 0.0, 0.0]
        sample_size[0] = self.sample_devices[self.active_sample_position].read_attribute("SizeX").value
        sample_size[1] = self.sample_devices[self.active_sample_position].read_attribute("SizeY").value
        sample_size[2] = self.sample_devices[self.active_sample_position].read_attribute("SizeZ").value
        return sample_size
        
        # PROTECTED REGION END #    //  TomoSamples.ActiveSampleSize_read

    def write_ActiveSampleSize(self, value):
        # PROTECTED REGION ID(TomoSamples.ActiveSampleSize_write) ENABLED START #
        
        if self.active_sample_position == "None":
            err_msg = "No active sample defined!"
            PyTango.Except.throw_exception("WriteError", err_msg, "write_ActiveSampleSize")
            
        self.sample_devices[self.active_sample_position].write_attribute("SizeX", value[0])
        self.sample_devices[self.active_sample_position].write_attribute("SizeY", value[1])
        self.sample_devices[self.active_sample_position].write_attribute("SizeZ", value[2])
        
        # PROTECTED REGION END #    //  TomoSamples.ActiveSampleSize_write

    def is_ActiveSampleSize_allowed(self, attr):
        # PROTECTED REGION ID(TomoSamples.is_ActiveSampleSize_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.DISABLE]
        else:
            return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_ActiveSampleSize_allowed

    def read_ActiveSampleAlignPos(self):
        # PROTECTED REGION ID(TomoSamples.ActiveSampleAlignPos_read) ENABLED START #
        
        if self.active_sample_position == "None":
            err_msg = "No active sample defined!"
            PyTango.Except.throw_exception("ReadError", err_msg, "read_ActiveSampleAlignPos")
        
        align_pos = [0.0, 0.0, 0.0]
        align_pos[0] = self.sample_devices[self.active_sample_position].read_attribute("AlignSX").value
        align_pos[1] = self.sample_devices[self.active_sample_position].read_attribute("AlignSY").value
        align_pos[2] = self.sample_devices[self.active_sample_position].read_attribute("AlignSZ").value
        return align_pos
        
        # PROTECTED REGION END #    //  TomoSamples.ActiveSampleAlignPos_read

    def write_ActiveSampleAlignPos(self, value):
        # PROTECTED REGION ID(TomoSamples.ActiveSampleAlignPos_write) ENABLED START #
        
        if self.active_sample_position == "None":
            err_msg = "No active sample defined!"
            PyTango.Except.throw_exception("WriteError", err_msg, "write_ActiveSampleAlignPos")
            
        self.sample_devices[self.active_sample_position].write_attribute("AlignSX", value[0])
        self.sample_devices[self.active_sample_position].write_attribute("AlignSY", value[1])
        self.sample_devices[self.active_sample_position].write_attribute("AlignSZ", value[2])
        
        # PROTECTED REGION END #    //  TomoSamples.ActiveSampleAlignPos_write

    def is_ActiveSampleAlignPos_allowed(self, attr):
        # PROTECTED REGION ID(TomoSamples.is_ActiveSampleAlignPos_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.DISABLE]
        else:
            return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_ActiveSampleAlignPos_allowed


    # --------
    # Commands
    # --------

    @DebugIt()
    def dev_state(self):
        # PROTECTED REGION ID(TomoSamples.State) ENABLED START #
        
        if len(self.tomo_samples) == 0:
            _state = PyTango.DevState.DISABLE
        else:
            _state = PyTango.DevState.ON
            
        self.set_state(_state)    
        return _state
        
        # PROTECTED REGION END #    //  TomoSamples.State

    @DebugIt()
    def dev_status(self):
        # PROTECTED REGION ID(TomoSamples.Status) ENABLED START #
        
        state = self.dev_state()
        
        self._status = "The device state is "
        if state == PyTango.DevState.DISABLE:
            self._status += "DISABLED"
            self._status += "\nNo samples defined! Add samples first."
        else:
            self._status += "ON"
        
        self.set_status(self._status)
        return self._status
        
        # PROTECTED REGION END #    //  TomoSamples.Status

    @command(
    dtype_in=('str',), 
    doc_in="Sampe position and sample name", 
    )
    @DebugIt()
    def AddSample(self, argin):
        # PROTECTED REGION ID(TomoSamples.AddSample) ENABLED START #
        
        position = argin[0].upper()
        
        # check for valid sample name
        # Column must be A, B or C
        column =  position[0]
        if column.isalpha() == False or \
           (column != "A" and column != "B" and column != "C"):
            err_msg = "Wrong sample name. The sample column in the shelf must be A, B or C"
            PyTango.Except.throw_exception("WriteError", err_msg, "AddSample")
        
        #row number must be between 1 and 18
        row = int(position[1:3])
        if row < 1 or row > 18:
            err_msg = "Wrong sample name. The sample row in the shelf must be between 1 and 18"
            PyTango.Except.throw_exception("WriteError", err_msg, "AddSample")
            
        # the samples A7 and A8 don't exist!!!
        if row == 7 or row == 8:
            err_msg = "The samples A,B,C 7 and 8 do not exist on the sample changer shelf!"
            PyTango.Except.throw_exception("WriteError", err_msg, "Addample")
        
        if position in self.tomo_samples.keys():
            err_msg = "A sample is already defined at position %s" % position
            PyTango.Except.throw_exception("WriteError", err_msg, "AddSample")
        
        self.sample_devices[position].write_attribute("Name", argin[1])
        self.tomo_samples[position] = argin[1]
        
        # PROTECTED REGION END #    //  TomoSamples.AddSample

    @command(
    dtype_in='str', 
    doc_in="Sample position or sample name", 
    )
    @DebugIt()
    def DeleteSample(self, argin):
        # PROTECTED REGION ID(TomoSamples.DeleteSample) ENABLED START #
        
        position = "None"
        if argin.upper() in self.tomo_samples.keys():
            position = argin.upper()
        else:
            for i in self.tomo_samples.keys():
                if self.tomo_samples[i].lower() == argin.lower():
                    position = i
                    break
        
        if position != "None":
            self.sample_devices[position].Reset()
            del self.tomo_samples[position]
            if self.active_sample_position == position:
                self.active_sample_position = "None"
                self.active_sample_name     = "None"
        else:
            err_msg = "Sample %s not found" % argin
            PyTango.Except.throw_exception("WriteError", err_msg, "DeleteSample")
        
        # PROTECTED REGION END #    //  TomoSamples.DeleteSample

    def is_DeleteSample_allowed(self):
        # PROTECTED REGION ID(TomoSamples.is_DeleteSample_allowed) ENABLED START #
        return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_DeleteSample_allowed

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(TomoSamples.Reset) ENABLED START #
        
        for i in list(self.tomo_samples.keys()):
            # do not reset samples with protected names
            found = False
            for psn in self.ProtectedSampleNames:
                if psn.lower() == self.tomo_samples[i].lower():
                    found = True;
                    break;
                    
            if found == False:
                self.sample_devices[i].Reset()
                del self.tomo_samples[i]
            
        self.active_sample_position = "None"
        self.active_sample_name     = "None"
        
        # PROTECTED REGION END #    //  TomoSamples.Reset

    def is_Reset_allowed(self):
        # PROTECTED REGION ID(TomoSamples.is_Reset_allowed) ENABLED START #
        return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_Reset_allowed

    @command(
    dtype_in='str', 
    doc_in="Sample position or sample name", 
    )
    @DebugIt()
    def ActivateSample(self, argin):
        # PROTECTED REGION ID(TomoSamples.ActivateSample) ENABLED START #
        
        position = "None"
        if argin.upper() in self.tomo_samples.keys():
            position = argin.upper()
        else:
            for i in self.tomo_samples.keys():
                if self.tomo_samples[i].lower() == argin.lower():
                    position = i
                    break
        
        if position != "None":
            self.active_sample_position = position
            self.active_sample_name     = self.tomo_samples[position]
        else:
            err_msg = "Sample %s not found, you need to define it first" % argin
            PyTango.Except.throw_exception("WriteError", err_msg, "ActivateSample")
        # PROTECTED REGION END #    //  TomoSamples.ActivateSample

    def is_ActivateSample_allowed(self):
        # PROTECTED REGION ID(TomoSamples.is_ActivateSample_allowed) ENABLED START #
        return self.get_state() not in [DevState.DISABLE]
        # PROTECTED REGION END #    //  TomoSamples.is_ActivateSample_allowed

    @command(
    dtype_in=('str',), 
    doc_in="Attribute name and its new value", 
    )
    @DebugIt()
    def EditSample(self, argin):
        # PROTECTED REGION ID(TomoSamples.EditSample) ENABLED START #

        name = argin[0]
        value = argin[1]
        
        if name == "Position":
            sample = []
            sample.append(value)
            sample.append(self.active_sample_name)
            self.AddSample(sample)
            prevSample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            newSample = PyTango.DeviceProxy (self.SamplePath + value)
            newSample.write_attribute_asynch("SizeX" , prevSample.SizeX)
            newSample.write_attribute_asynch("SizeY" , prevSample.SizeY)
            newSample.write_attribute_asynch("SizeZ" , prevSample.SizeZ)
            newSample.write_attribute_asynch("AlignSX" , prevSample.AlignSX)
            newSample.write_attribute_asynch("AlignSY" , prevSample.AlignSY)
            newSample.write_attribute_asynch("AlignSZ" , prevSample.AlignSZ)
            prevSample.Reset()
            self.init_device()
            self.ActivateSample(newSample.Position)
        elif name == "Name":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("Name" , value)
            self.init_device()
        elif name == "SizeX":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("SizeX" , float(value))
        elif name == "SizeY":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("SizeY" , float(value))
        elif name == "SizeZ":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("SizeZ" , float(value))
        elif name == "AlignSX":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("AlignSX" , float(value))
        elif name == "AlignSY":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("AlignSY" , float(value))
        elif name == "AlignSZ":
            sample = PyTango.DeviceProxy (self.SamplePath + self.active_sample_position)
            sample.write_attribute_asynch("AlignSZ" , float(value))
            
        # PROTECTED REGION END #    //  TomoSamples.EditSample

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(TomoSamples.main) ENABLED START #
    
    return run((TomoSample.TomoSample, TomoSamples,), args=args, **kwargs)
    
    # PROTECTED REGION END #    //  TomoSamples.main

if __name__ == '__main__':
    main()
